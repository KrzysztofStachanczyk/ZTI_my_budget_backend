SELECT T.value AS tagName, COUNT(*) AS operationsCount, SUM(F.amount) AS amountSum
FROM Finance_Operations F
LEFT JOIN Tags_Attribution TA ON TA.operationid=F.operationid
LEFT JOIN Tags T ON T.tagid=TA.tagid
WHERE fuid = 3
GROUP BY T.tagid
package com.zti.myBudget.QueryBO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * Obiekt POJO zawierający statystyki dotyczące wybranego tagu.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BasicStatisticsOfTag {
    /**
     * Nazwa tagu.
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    private String tagName;

    /**
     * Ilość operacji finansowych.
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    private Long operationsCount;

    /**
     * Bilans
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    private Double amountSum;

    /**
     * Suma przychodów.
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    private Double incomeSum;

    /**
     * Suma rozchodów.
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    private Double outcomeSum;
}

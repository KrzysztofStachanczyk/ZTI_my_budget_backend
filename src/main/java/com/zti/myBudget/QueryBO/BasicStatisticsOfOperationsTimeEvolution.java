package com.zti.myBudget.QueryBO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

/**
 * Obiekt POJO zawierający uśrednione statystyki z wybranego okresu czasowego dla określonej jednostki finansowej.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BasicStatisticsOfOperationsTimeEvolution {
    /**
     * Początek okresu z którego generowane jest podsumowanie
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    private LocalDate periodBegin;

    /**
     * Ilość operacji
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    private Long operations;

    /**
     * Bilans operacji
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    private Double amountSum;

    /**
     * Suma przychodów
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    private Double incomeSum;

    /**
     * Suma rozchodów
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    private Double outcomeSum;
}

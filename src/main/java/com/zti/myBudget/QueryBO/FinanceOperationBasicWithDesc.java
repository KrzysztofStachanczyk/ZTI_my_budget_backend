package com.zti.myBudget.QueryBO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Obiekt POJO reprezentujący podstawowe dane operacji finansowej włącznie z opisem.
 * Jest wynikiem projekcji encji 'FinanceUnitOperation'.
 */
@Getter
@Setter
@AllArgsConstructor
public class FinanceOperationBasicWithDesc {
    /**
     *  Identyfikator operacji finansowej
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    private Long operationId;

    /**
     * Nazwa operacji finansowej
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    private String title;

    /**
     * Wartość operacji finansowej w domyślnej walucie
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    private BigDecimal amount;

    /**
     * Data operacji
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    private LocalDate execDate;

    /**
     * Data waluty
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    private LocalDate orderDate;

    /**
     * Opis operacji finansowej
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    private String description;
}


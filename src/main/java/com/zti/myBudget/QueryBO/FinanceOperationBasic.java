package com.zti.myBudget.QueryBO;

import com.zti.myBudget.BO.FinanceUnit;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashMap;

/**
 * Obiekt POJO reprezentujący podstawowe dane operacji finansowej.
 * Jest wynikiem projekcji encji 'FinanceUnitOperation'.
 */
@Getter
@Setter
@AllArgsConstructor
public class FinanceOperationBasic {
    /**
     *  Identyfikator operacji finansowej
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    private Long operationId;

    /**
     * Nazwa operacji finansowej
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    private String title;

    /**
     * Wartość operacji finansowej w domyślnej walucie
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    private BigDecimal amount;

    /**
     * Data operacji
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    private LocalDate execDate;

    /**
     * Data waluty
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    private LocalDate orderDate;

}

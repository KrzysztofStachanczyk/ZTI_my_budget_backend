package com.zti.myBudget.DAO;

import com.zti.myBudget.BO.ControllersCalls;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Repozytorium dostarczające zbiór podstawowych operacji do pobierania
 * i zapisu obiektów klasy "ControllersCalls" (lub pochodnych) do bazy danych.
 */
@Transactional(readOnly = true)
@Repository
public interface ControllersCallsRepository extends JpaRepository<ControllersCalls,String> {

}

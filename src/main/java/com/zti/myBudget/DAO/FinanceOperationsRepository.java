package com.zti.myBudget.DAO;

import com.zti.myBudget.BO.FinanceOperation;
import com.zti.myBudget.QueryBO.FinanceOperationBasic;
import com.zti.myBudget.QueryBO.FinanceOperationBasicWithDesc;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

/**
 * Repozytorium JPA dla operacji finansowych. Jest to podstawowy interfejs DAO dla obiektów klasy FinanceOperation.
 */
@Transactional(readOnly = true)
@Repository
public interface FinanceOperationsRepository extends JpaRepository<FinanceOperation,Long> {
    /**
     * Zapytanie pozwalające na odczyt podstawowych danych o operacjach finansowych.
     * @param fuId identyfikator jednostki finansowej z której pobierane są operacje
     * @return zbiór obiektów POJO klasy FinanceOperationBasic
     */
    @Query("select new com.zti.myBudget.QueryBO.FinanceOperationBasic(o.operationId,o.title,o.amount,o.execDate,o.orderDate) from FinanceOperation o where o.financeUnit.id = ?1")
    Set<FinanceOperationBasic> findAllOperationsBasicInfo(Long fuId);

    /**
     * Zapytanie odczytujące z bazy danych informacje o operacjach finansowych z wybranego okna czasowego.
     * @param fuId  identyfikator jednostki finansowej (okna)
     * @param fromDate  data początkowa
     * @param toDate  data końcowa
     * @return lista obiektów POJO klasy FinanceOperationBasicWithDesc
     */
    @Query("select new com.zti.myBudget.QueryBO.FinanceOperationBasicWithDesc(o.operationId,o.title,o.amount,o.execDate,o.orderDate,o.description) from FinanceOperation o where o.financeUnit.id = ?1 and o.orderDate >= ?2 and o.orderDate <= ?3 order by orderDate")
    List<FinanceOperationBasicWithDesc> findAllOperationsInTimeRange(Long fuId, LocalDate fromDate,LocalDate toDate);
}



package com.zti.myBudget.DAO;

import com.zti.myBudget.BO.Tag;
import com.zti.myBudget.BO.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Repozytorium JPA dla tagów. Jest to podstawowy interfejs DAO dla obiektów klasy Tag.
 */
@Transactional(readOnly = true)
@Repository
public interface TagsRepository  extends JpaRepository<Tag, Long> {
}

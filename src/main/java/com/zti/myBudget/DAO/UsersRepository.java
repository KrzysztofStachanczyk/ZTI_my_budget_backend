package com.zti.myBudget.DAO;

import com.zti.myBudget.BO.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Repozytorium JPA dla użytkowników. Jest to podstawowy interfejs DAO dla obiektów klasy User.
 */
@Transactional(readOnly = true)
@Repository
public interface UsersRepository extends JpaRepository<User, Long> {
    /**
     * Wyszukuje użytkownika na podstawie adresu email.
     * @param email - adres email
     * @return - obiekt Optional dla klasy User
     */
    Optional<User> findByEmail(String email);
}
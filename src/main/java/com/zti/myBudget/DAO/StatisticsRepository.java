package com.zti.myBudget.DAO;

import com.zti.myBudget.QueryBO.BasicStatisticsOfOperationsTimeEvolution;
import com.zti.myBudget.QueryBO.BasicStatisticsOfTag;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;


/**
 * Repozytorium zawierające zapytania umożliwiające generowanie statystyk. Nie jest ono związane z kontkretną klasą (encją).
 * Wykonuje tylko operacje READ z wykorzystaniem złączeń i funkcji agregujących zwracając odpowiednio przygotowane obiekty POJO.
 */
@Repository
@Transactional(readOnly = true)
public class StatisticsRepository {
    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Generuje statystyki związane z tagami dla wybranej jednostki finansowej w tym:
     * - ilość wystąpień tagu
     * - podsumowanie operacji z tym samym tagiem
     * - podsumowanie przychodów
     * - podsumowanie rozchodów
     * @param fuId identyfikator jednostki finansowej (konta)
     * @return  lista obiektów POJO klasy BasicStatisticsOfTag
     */
    public List<BasicStatisticsOfTag> findBasicStatisticsOfTags(Long fuId){
        Session session = entityManager.unwrap(Session.class);

        TypedQuery<BasicStatisticsOfTag>  query= session.createNativeQuery("SELECT T.value AS tagname, COUNT(*) AS operationscount, SUM(F.amount) AS amountsum, "+
                "SUM(CASE WHEN F.amount >= 0 then F.amount else 0 END) AS incomesum, " +
                "SUM(CASE WHEN F.amount <= 0 then -F.amount else 0 END) AS outcomesum " +
                "FROM Finance_Operations F " +
                "LEFT JOIN Tags_Attribution TA ON TA.operationid=F.operationid " +
                "LEFT JOIN Tags T ON T.tagid=TA.tagid " +
                "WHERE fuid = :fuid " +
                "GROUP BY T.tagid","FinanceBasicStatisticsMapping").setParameter("fuid",fuId);

        return  query.getResultList();
    }

    /**
     * Generuje statystyki związane z tagami dla wybranej jednostki finansowej w określonym oknie czasowym.
     * @param fuId identyfikator jednostki finansowej (konta)
     * @param from data początkowa
     * @param to data końcowa
     * @return lista obiektów POJO klasy BasicStatisticsOfTag
     */
    public List<BasicStatisticsOfTag> findBasicStatisticsInTimeRange(Long fuId, LocalDate from, LocalDate to){
        Session session = entityManager.unwrap(Session.class);

        TypedQuery<BasicStatisticsOfTag>  query= session.createNativeQuery("SELECT T.value AS tagname, COUNT(*) AS operationscount, SUM(F.amount) AS amountsum, "+
                "SUM(CASE WHEN F.amount >= 0 then F.amount else 0 END) AS incomesum, " +
                "SUM(CASE WHEN F.amount <= 0 then -F.amount else 0 END) AS outcomesum " +
                "FROM Finance_Operations F " +
                "LEFT JOIN Tags_Attribution TA ON TA.operationid=F.operationid " +
                "LEFT JOIN Tags T ON T.tagid=TA.tagid " +
                "WHERE fuid = :fuid AND F.order_date >= :fromDate AND F.order_date <= :toDate " +
                "GROUP BY T.tagid","FinanceBasicStatisticsMapping")
                .setParameter("fuid",fuId)
                .setParameter("fromDate",from)
                .setParameter("toDate",to);

        return  query.getResultList();
    }

    /**
     * Oblicza uśrednione statystyki dotyczące przychodów, rozchodów i ilości operacji w okresach o zadanej długości.
     * @param fuId  identyfikator jednostki finansowej
     * @param truncType  nazwa okresu czasowego 'day','week','month','quarter','year'
     * @return lista obiektów POJO klasy BasicStatisticsOfOperationsTimeEvolution
     */
    public List<BasicStatisticsOfOperationsTimeEvolution> findEvolutionBasic(Long fuId,String truncType){
        Session session = entityManager.unwrap(Session.class);

        TypedQuery<BasicStatisticsOfOperationsTimeEvolution>  query= session.createNativeQuery(
                "SELECT " +
                "  date_trunc(:truncType, F.order_date)\\:\\:DATE AS periodbegin, " +
                "  COUNT(amount) AS operations, " +
                "  SUM(amount) AS amountsum, " +
                "  SUM(CASE WHEN F.amount >= 0 then F.amount else 0 END) AS incomesum, " +
                "  SUM(CASE WHEN F.amount <= 0 then -F.amount else 0 END) AS outcomesum " +
                "FROM Finance_Operations AS F " +
                "WHERE fuID = :fuid " +
                "GROUP BY periodbegin " +
                "ORDER BY periodbegin","BasicStatisticsOfOperationsTimeEvolutionMapping")
                .setParameter("fuid",fuId).setParameter("truncType",truncType);

        return  query.getResultList();
    }

    // Niestety nie można zdefiowaś mapowania z native query bez entity zatem tworze fikcyjne
    @SqlResultSetMapping(
            name = "FinanceBasicStatisticsMapping",
            classes = @ConstructorResult(
                    targetClass = com.zti.myBudget.QueryBO.BasicStatisticsOfTag.class,
                    columns = {
                            @ColumnResult(name = "tagname",type = String.class),
                            @ColumnResult(name = "operationscount",type = Long.class),
                            @ColumnResult(name = "amountsum", type = Double.class),
                            @ColumnResult(name = "incomesum",type = Double.class),
                            @ColumnResult(name = "outcomesum",type = Double.class)
                    }))
    @Entity class SQLMappingCfgEntity{@Id int id;}

    @SqlResultSetMapping(
            name = "BasicStatisticsOfOperationsTimeEvolutionMapping",
            classes = @ConstructorResult(
                    targetClass = com.zti.myBudget.QueryBO.BasicStatisticsOfOperationsTimeEvolution.class,
                    columns = {
                            @ColumnResult(name = "periodbegin",type = java.time.LocalDate.class),
                            @ColumnResult(name = "operations",type = Long.class),
                            @ColumnResult(name = "amountsum",type = Double.class),
                            @ColumnResult(name = "incomesum", type = Double.class),
                            @ColumnResult(name = "outcomesum",type = Double.class)
                    }))
    @Entity class SQLMappingCfgEntity2{@Id int id;}
}

package com.zti.myBudget.DAO;

import com.zti.myBudget.BO.FinanceUnit;
import com.zti.myBudget.BO.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;


/**
 * Repozytorium JPA dla jednostek finansorych. Jest to podstawowy interfejs DAO dla obiektów klasy FinanceUnit.
 */
@Transactional(readOnly = true)
@Repository
public interface FinanceUnitsRepository  extends JpaRepository<FinanceUnit, Long> {
    /**
     * Zapytanie pozwalające wyszukać jednostki przypisane do użytkownika.
     * @param owner użytkownik (właściciel) jednostki finansowej
     * @return zbiór podległych jednostek finansowych
     */
    Set<FinanceUnit> findByOwner(User owner);
}
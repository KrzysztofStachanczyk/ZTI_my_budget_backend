package com.zti.myBudget.Validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.annotation.Annotation;
import java.util.regex.Pattern;

/**
 * Walidator sprawdzający poprawność podanego przy rejestracji hasła. To jest w formacie:
 * - 8-20 znaków
 * - conajmniej jedna: mała litera, duża litera, cyfra
 */
public class PasswordValidator implements ConstraintValidator<PasswordConstraints, String> {
    private static Pattern pattern;

    static{
        pattern=Pattern.compile("(?=.*[a-z])(?=.*[0-9])(?=.*[A-Z]).{8,20}");
    }

    /**
     * Metoda implementująca procedurę sprawdzającą poprawność przekazanego ciągu znaków.
     * @param object  hasło którego poprawność należy sprawdzić
     * @param constraintContext
     * @return czy hasło spełnia wymagane kryteria
     */
    @Override
    public boolean isValid(String object, ConstraintValidatorContext constraintContext) {
        return pattern.matcher(object).matches();
    }

}
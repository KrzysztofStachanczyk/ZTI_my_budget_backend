package com.zti.myBudget.FormsBO;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Obiekt POJO reprezentujący dane pochodzące z formularzy dodawania i modyfikacji operacji finansowych.
 */
@Setter
@Getter
public class FinanceOperationAddOrModify
{

    /**
     *  Tytuł operacji - nie dłuższy niż 100 znaków i nie krótszy od 3
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    @NotNull
    @Size(min=3,max=100,message = "Title should have [3,100] characters")
    private String title;

    /**
     * Opis operacji finansowej - jego długość jest ograniczona do 1000 znaków
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    @NotNull
    @Size(max=1000,message = "Description should have less than 1000 characters")
    private String description;

    /**
     *  Wartość operacji finansowej - do pietnastu cyfr znaczących i dwóch miejsc po przecinku
     *  (zapis stałoprzecinkowy)
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    @NotNull
    @Digits(integer = 15,fraction = 2, message = "Incorrect money format")
    private BigDecimal amount;

    /**
     *  Data zaksięgowania operacji (waluty).
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    @NotNull
    private LocalDate execDate;

    /**
     *  Data zlecenia wykonania operacji.
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    @NotNull
    private LocalDate orderDate;
}

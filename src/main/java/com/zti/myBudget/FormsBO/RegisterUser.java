package com.zti.myBudget.FormsBO;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.zti.myBudget.Validators.PasswordConstraints;
import lombok.Getter;

/**
 * Obiekt POJO reprezentujący dane pochodzące z formularza rejestracyjnego.
 */
@Getter
public class RegisterUser {
    /**
     * Email - musi byś unikalny w systemie, a jego długość nie może być większa niż 70 znaków.
     *  -- GETTER --
     *  Domyślny getter
     */
    @NotNull
    @Email
    @Size(max=70,message = "Email is too long")
    private String email;

    /**
     * Imię użytkownika - nie może być dłuższe niż 100 znaków.
     *  -- GETTER --
     *  Domyślny getter
     */
    @NotNull
    @Size(min=3,max=100,message = "Name is to short")
    private String name;

    /**
     * Hasło do konta. Musi mieć co najmniej 8 znaków. Zawierać małe, wielkie litery i cyfry.
     *  -- GETTER --
     *  Domyślny getter
     */
    @NotNull
    @Size(min=8, message="Password is to short")
    @PasswordConstraints
    private String password;

    /**
     * Nazwisko użytkownika - nie może być dłuższe niż 250 znaków.
     *  -- GETTER --
     *  Domyślny getter
     */
    @NotNull
    @Size(min=3,message = "Surname is to short")
    private String surname;


}

package com.zti.myBudget.FormsBO;

import lombok.Getter;
import lombok.Setter;

/**
 * Obiekt POJO reprezentujący dane pochodzące z formularza logowania.
 */
@Getter
public class AuthUser {
    /**
     * Hasło użytkownika
     *  -- GETTER --
     *  Domyślny getter
     */
    private String password;

    /**
     * Email użytkownika
     *  -- GETTER --
     *  Domyślny getter
     */
    private String email;
}

package com.zti.myBudget.FormsBO;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Obiekt POJO reprezentujący dane pochodzące z formularza dodawania tagów.
 */
@Getter
public class NewTag {
    /**
     * Nazwa tagu - nie może przekraczać 30 znaków, ani być krótsza od 3.
     *  -- GETTER --
     *  Domyślny getter
     */
    @NotNull
    @Size(min = 3,max = 30, message = "Tag name on in range [3,30]")
    private String name;

}

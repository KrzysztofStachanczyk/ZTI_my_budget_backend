package com.zti.myBudget.FormsBO;

import lombok.Getter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Obiekt POJO reprezentujący dane pochodzące z formularza dodawania jednostki finansowej (np. konta).
 */
@Getter
public class FinanceUnitAdd {
    /**
     * Nazwa jednostki finansowej - nie dłuższa niż 50 znaków, nie krótsza od 3
     *  -- GETTER --
     *  Domyślny getter
     */
    @NotNull
    @Size(min = 3, max = 50, message = "Finance unit name size should be in range [3,50]")
    private String name;

    /**
     * Opcjonalny opis jednostki finansowej. Maksymalna długość to 500 znaków
     *  -- GETTER --
     *  Domyślny getter
     */
    @Size(max = 500,message = "Description should have less than 500 letters")
    private String description;

}

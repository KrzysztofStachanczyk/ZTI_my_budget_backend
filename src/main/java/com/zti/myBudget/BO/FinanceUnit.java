package com.zti.myBudget.BO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Set;
import javax.persistence.Id;
import javax.persistence.IdClass;

/**
 * Klasa której obiekty reprezentują jednostki finansowe, na przykład lokatę, rachunek oszczędnościowy
 * czy konto bankowe. W bazie reprezentowana jest przez tabelę "Finance_Units".
 */
@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name="Finance_Units",uniqueConstraints = {@UniqueConstraint(columnNames = {"ownerid","name"})})
public class FinanceUnit {
    /**
     * Unikalny identyfikator jednostki finansowej.
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="fuID",nullable = false)
    private Long id;

    /**
     * Nazwa jednostki finansowej - nie dłuższa niż 50 znaków
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    @NotNull
    @Column(length = 50,nullable = false)
    private String name;

    /**
     * Referencja do obiektu reprezentującego użytkownika - właściciela jednostki finansowej.
     * Pobierana jest dynamicznie w momencie pierwszego wywołania gettera - 'LAZY initialization'.
     * Nie jest dołączana do dokumentu JSON reprezentuijącego obiekt.
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="ownerid", referencedColumnName="userid",nullable = false)
    private User owner;

    /**
     * Opcjonalny opis jednostki finansowej. Maksymalna długość to 500 znaków
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    @Column(length = 500)
    private String description;


    public FinanceUnit(@NotNull Long id, @NotNull String name, @NotNull User user, @NotNull String description) {
        this.id = id;
        this.name = name;
        this.owner = user;
        this.description = description;
    }

    /**
     * Referencja do zbioru operacji finansowych przypisanych wybranej jednostwe finansowej. Zbiór ten pobierany
     * jest na żądanie z bazy danych. Usunięcie lub modyfikacja jednostki finansowej powoduje propagowanie się zmiany
     * do podległych jej operacji. W przypadku usunięcia podległe operacje zostają usunięte. Pole to nie podlega serializacji
     * do dokumentu JSON.
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL,orphanRemoval = true,mappedBy = "financeUnit")
    public Set<FinanceOperation> financeOperations;

}

package com.zti.myBudget.BO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zti.myBudget.FormsBO.FinanceOperationAddOrModify;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;


/**
 *  Klasa której obiekty reprezentują elementarne operacje finansowe.
 *  Odpowiada tabeli "Finance_Operations" w bazie danych.
 *  Format i nazewnictwo atrybutów zostało zaczerpnięte z raportu finansowego PKO.
 *
 *  Wartości atrybutów execDate,orderDate,title,amount wraz z referencją na jednostke finansową tworzą klucz unikalny.
 */
@Getter
@Entity
@Setter
@Table(name = "Finance_Operations",uniqueConstraints = {@UniqueConstraint(columnNames = {"fuID","exec_date","order_date","title","amount"})})
@NoArgsConstructor
public class FinanceOperation {

    /**
     *  Unikalny identyfikator operacji.
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "operationID",nullable = false)
    private Long operationId;

    /**
     *  Data zaksięgowania operacji (waluty).
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    @NonNull
    @Column(name = "exec_date",nullable = false)
    private LocalDate execDate;

    /**
     *  Data zlecenia wykonania operacji.
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    @NotNull
    @Column(name = "order_date",nullable = false)
    private LocalDate orderDate;

    /**
     *  Tytuł operacji - nie dłuższy niż 100 znaków
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    @NotNull
    @Column(length = 100,nullable = false)
    private String title;

    /**
     * Opis operacji finansowej - może nie występować (null), jego długość jest ograniczona do 1000 znaków
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    @Column(length = 1000)
    private String description;

    /**
     *  Wartość operacji finansowej - do pietnastu cyfr znaczących i dwóch miejsc po przecinku
     *  (zapis stałoprzecinkowy)
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    @NotNull
    @Column(nullable= false, precision=15, scale=2)
    private BigDecimal amount;


    /**
     *  Referencja do jednostki finansowej której podlega operacja. Obiekt jednostki finansowej jest pobierany
     *  z bazy danych na żądanie. Pole jest ignorowane przy konwersji do dokumentu JSON.
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    @NotNull
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="fuID",nullable = false)
    private FinanceUnit financeUnit;

    /**
     *  Referencja do zbioru tagów przypisanych do operacji - tagi są usuwane automatycznie w momencie usunięcia operacji i
     *  pobierane w czasie tworzenia obiektu.
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    @ManyToMany(fetch = FetchType.EAGER,cascade = CascadeType.MERGE)
    @JoinTable(name="Tags_Attribution",joinColumns = @JoinColumn(name = "operationID",referencedColumnName = "operationID"),
            inverseJoinColumns = @JoinColumn(name="tagid",referencedColumnName = "tagid"))
    private Set<Tag> tags;


    /**
     * Konstruktor tworzący obiekt na podstawie zwalidowanego obiektu POJO reprezentującego nową operację
     * i jednostki finansowej do której ma należeć.
     * @param data  opis nowej operacji dostarczony przez klienta
     * @param fu  jednostka finansowa
     */
    public FinanceOperation(FinanceOperationAddOrModify data,FinanceUnit fu){
        this.execDate=data.getExecDate();
        this.orderDate=data.getOrderDate();
        this.title=data.getTitle();
        this.description=data.getDescription();
        this.amount=data.getAmount();
        this.financeUnit=fu;
    }


}

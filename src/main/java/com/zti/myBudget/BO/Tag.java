package com.zti.myBudget.BO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * Klasa której obiekty reprezentują pojedyncze tagi. Tagi mogą być dopinane do operacji finansowych. Odpowiada tabeli
 * "Tags" w relacyjnej bazie danych. Użytkownik może stworzyć tylko jeden o określonej nazwie.
 */
@Getter
@Entity
@NoArgsConstructor
@Table(name="Tags",uniqueConstraints = {@UniqueConstraint(columnNames = {"ownerid","value"})})
public class Tag {

    /**
     * Unikalny identyfikator tagu.
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "tagid",nullable = false)
    private Long id;

    /**
     * Referencja na obiekt właściciela (twórcy) tagu. Szczegóły dotyczące tego użytkownika
     * mogą zostać pobrane na żądanie (getter) z bazy danych. Atrybut ten nie podlega konqwersji do dokumentu JSON
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    @NotNull
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="ownerid", referencedColumnName="userid",nullable = false)
    private  User owner;

    /**
     * Nazwa tagu - nie może przekraczać 30 znaków.
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    @NotNull
    @Column(length = 30,nullable = false)
    private String value;

    /**
     * Konstruktor tagu
     * @param user użytkownik - właściciel tagu
     * @param value nazwa tagu
     */
    public Tag(@NotNull User user, @NotNull String value) {
        this.owner = user;
        this.value = value;
    }

    /**
     * Referencja do zbioru operacji finansowych, które zostały przypisane do tagu.
     * W przypadku jego usunięcia operacje te nie są kasowane, a znika tylko asocjacja pomiędzy nimi, a
     * nie istniejącym już tagiem. Zbiór ten pobierany jest na żądanie przy pierwszym odwołaniu do niego i
     * nie podlega konwersji do dokumentu JSON.
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    @JsonIgnore
    @ManyToMany(mappedBy = "tags",fetch = FetchType.LAZY,cascade = CascadeType.MERGE)
    private Set<FinanceOperation> financeOperations;
}


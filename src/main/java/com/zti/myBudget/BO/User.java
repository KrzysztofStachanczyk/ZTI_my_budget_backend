package com.zti.myBudget.BO;

import com.zti.myBudget.FormsBO.RegisterUser;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Set;

/**
 * Klasa której obiekty reprezentują użytkowników systemu. Zawiera informacje o podstawowych danych osobowych, loginie
 * (email) i haśle. Odpowiada tabeli "Users" w bazie danych.
 */
@Getter
@Entity
@Table(name="Users")
@NoArgsConstructor
public class User {

    /**
     * Enkoder do haseł pozwalający wykorzystujący funkcje skrótu.
     */
    private static  PasswordEncoder passwordEncoder;

    static {
        passwordEncoder = new BCryptPasswordEncoder();
    }

    /**
     * Konstruktor tworzący nowego użytkownika na podstawie obiektu POJO dostarczonego w czasie
     * rejestracji.
     * @param user - obiekt POJO zawierający dane z formularza rejestracyjnego.
     */
    public User(@NotNull RegisterUser user) {
        this.email = user.getEmail();
        this.password_sha_256 = passwordEncoder.encode(user.getPassword());
        this.name = user.getName();
        this.surname = user.getSurname();
    }

    /**
     * Metoda pozwalająca sprawdzić, czy hasło w postaci jawnej jest zgodne z zaszyfrowanym.
     * @param password hasło do konta użytkownika
     * @return prawda jeżeli wygenerowany hash jest zgodny z przypisanym do konta
     */
    public boolean checkPassword(@NotNull String password){
        return passwordEncoder.matches(password,password_sha_256);
    }

    /**
     * Unikalny identyfikator użytkownika.
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column (name = "userid",nullable = false)
    private Long userID;

    /**
     * Email - musi byś unikalny w systemie, a jego długość nie może być większa niż 70 znaków.
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    @NotNull
    @Column(unique = true,length = 70,nullable = false)
    private String email;

    /**
     * Hash hasła użytkownika uzyskiwany z wykorzystaniem funkcji skrótu SHA-256. Nie podlega konwersji do dokumentu
     * JSON przesyłanego do klienta.
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    @Getter(AccessLevel.NONE)
    @NotNull
    @JsonIgnore
    @Column(nullable = false)
    private String password_sha_256;

    /**
     * Imię użytkownika - nie może być dłuższe niż 100 znaków.
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    @NotNull
    @Column(length = 100,nullable = false)
    private String name;

    /**
     * Nazwisko użytkownika - nie może być dłuższe niż 250 znaków.
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    @NotNull
    @Setter
    @Column(length = 250,nullable = false)
    private String surname;

    /**
     * Referencja do należących do użytkownika jednostek finansowych. Wczytywana na żądanie (wywołanie getter).
     * Nie podlega serializacji do obiektu JSON reprezentującego użytkownika. Wprowadzanie zmian (w tym usuwanie) jest
     * propagowane do podlegających mu jednostek.
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL,orphanRemoval = true,mappedBy = "owner")
    private Set<FinanceUnit> financeUnits;

    /**
     * Referencja do stworzonych przez użytkownika tagów. Wczytywana z bazy danych 'ad-hoc' w razie potrzeby.
     * Wprowadzanie zmian (usuwanie, zmiana PK) jest propagowane na tagi. Atrybyt nie podlega serializacji do
     * dokumentu JSON opisującego obiekt.
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL,orphanRemoval = true,mappedBy = "owner")
    private Set<Tag> tags;

}

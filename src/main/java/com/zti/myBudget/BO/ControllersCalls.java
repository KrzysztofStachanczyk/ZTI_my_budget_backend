package com.zti.myBudget.BO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Klasa której obiekty reprezentują informacje o wkorzystaniu poszczególnych punktów wejścia kontrolerów REST.
 * W bazie danych reprezentowana jest przez tabelę "controllers_calls". Statystyki zbierane są z pomocą programowania
 * aspektowego.
 */
@Entity
@Table(name = "controllers_calls")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ControllersCalls {
    /**
     * Unikalny identyfikator obiektu. Zawiera on pełną nazwę klasy oraz nazwę wywoływanej metody w formacie:
     * com.zti.myBudget.[class name].[method name]. W projekcie nie występuje przeciążenie metod kontrolerów.
     * Górnym ograniczeniem na długość powstałego łańcucha znaków jest 512 elementów.
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    @Id
    @Column(length = 512, nullable = false)
    private String endpointMethod;

    /**
     * Liczba wywołań metody od momentu zainicjalizowania bazy danych. Początkowa jej wartość wynosi zero.
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    @Column(nullable = false)
    private Long callNumber;

    /**
     * Średni czas wykonywania metody (obsługi żądania) wyrażony w sekundach.
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    @Column(nullable = false)
    private Double averageExecutionTime;

    /**
     * Liczba nieprzechwyconych przez kod kontrolera wyjątków.
     *  -- SETTER --
     *  Domyślny setter
     *  -- GETTER --
     *  Domyślny getter
     */
    @Column(nullable = false)
    private Long exceptionsNumber;

    /**
     * Konstruktor domyślny
     * @param endpointMethod - nazwa punktu wejścia w formacie com.zti.myBudget.[class name].[method name]
     */
    public ControllersCalls(String endpointMethod) {
        this.endpointMethod = endpointMethod;
        this.callNumber= new Long(0);
        this.averageExecutionTime= new Double(0);
        this.exceptionsNumber = new Long(0);
    }
}

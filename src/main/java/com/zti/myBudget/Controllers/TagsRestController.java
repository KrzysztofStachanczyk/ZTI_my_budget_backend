package com.zti.myBudget.Controllers;

import com.zti.myBudget.BO.Tag;
import com.zti.myBudget.BO.User;
import com.zti.myBudget.DAO.TagsRepository;
import com.zti.myBudget.DAO.UsersRepository;
import com.zti.myBudget.FormsBO.NewTag;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;
import java.util.Set;

/**
 * Kontroler REST obsługujący manipulację tagami.
 */
@RestController
public class TagsRestController {
    /**
     * Repozytorium tagów.
     */
    @Autowired
    TagsRepository tagsRepository;

    /**
     * Repozytorium użytkowników
     */
    @Autowired
    UsersRepository usersRepository;

    /**
     * Pobiera tagi przypisane do użytkownika, którego identyfikator znajsuje się w tokenie.
     * Entrypoint opisywanej metody to GET["/secure/tags"].
     * Przekazane żądanie powinno zawierać:
     * <ul>
     *  <li> token autoryzacyjny </li>
     * </ul>
     * @param claims dane z odszyfrowanego przez filtr tokenu, w tym id użytkownika, email i data wygenerowania
     * @return obiekt ResponseEntity zwracany do klienta jako HTTP response o następujących kodach:
     * <ul>
     *  <li> 404 - użytkownik o identyfikatorze zapisanym w tokenie nie istnieje </li>
     *  <li> 200 - pobranie tagów przebiegło pomyślnie </li>
     *  </ul>
     * W przypadku kodu 200 zwracany jest zbiór zserializowanych do dokumentów
     * JSON obiektów POJO klasy Tag reprezentujacych tagi stworzone przez użytkownika.
     */
    @RequestMapping(value = "/secure/tags",method = RequestMethod.GET)
    public ResponseEntity<?> getAll(@RequestAttribute("claims") Claims claims){
        Long ownerID = new Long((Integer)claims.get("user_id"));
        Optional<User> userOptional = usersRepository.findById(ownerID);

        if(!userOptional.isPresent()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        Set<Tag> tags = userOptional.get().getTags();
        return new ResponseEntity<>(tags,HttpStatus.OK);
    }

    /**
     * Dodaje nowy tag dla użytkownika, którego identyfikator znajduje się w tokenie.
     * Entrypoint opisywanej metody to POST["/secure/tags"].
     * Przekazane żądanie powinno zawierać:
     * <ul>
     *  <li> token autoryzacyjny </li>
     *  <li> mapowalny na obiekt POJO klasy NewTag dokument JSON </li>
     * </ul>
     * @param claims dane z odszyfrowanego przez filtr tokenu, w tym id użytkownika, email i data wygenerowania
     * @param newTag obiekt POJO klasy NewTag zawierający atrybuty wymagane do stworzenia nowego tagu
     * @return obiekt ResponseEntity zwracany do klienta jako HTTP response o następujących kodach:
     * <ul>
     *  <li> 404 - użytkownik o identyfikatorze zapisanym w tokenie nie istnieje </li>
     *  <li> 200 - tworzenie tagu przebiegło pomyślnie </li>
     *  <li> 409 - tag o podanej nazwie już istnieje dla tego użytkownika </li>
     *  <li> 400 - błąd walidacji, odwzorowania JSON- &gt; POJO </li>
     *  </ul>
     */
    @RequestMapping(value = "/secure/tags",method = RequestMethod.POST)
    public ResponseEntity<?> insertNewTag(@RequestAttribute("claims") Claims claims,@Valid @RequestBody NewTag newTag){
        Long ownerID = new Long((Integer) claims.get("user_id"));

        Optional<User> userOptional = usersRepository.findById(ownerID);

        if(!userOptional.isPresent()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        Tag tag = new Tag(userOptional.get(),newTag.getName());

        try {
            tagsRepository.saveAndFlush(tag);
        }catch (org.springframework.dao.DataIntegrityViolationException e){
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }

        return new ResponseEntity<>(tag,HttpStatus.OK);
    }

    /**
     * Usuwa tag o zadanym identyfikatorze.
     * Entrypoint opisywanej metody to DELETE["/secure/tags/{id}"].
     * Przekazane żądanie powinno zawierać:
     * <ul>
     *  <li> token autoryzacyjny </li>
     *  <li> zapisany w adresie URL identyfikator tagu </li>
     * </ul>
     * @param claims dane z odszyfrowanego przez filtr tokenu, w tym id użytkownika, email i data wygenerowania
     * @param tagId identyfikator tagu
     * @return obiekt ResponseEntity zwracany do klienta jako HTTP response o następujących kodach:
     * <ul>
     *  <li> 404 - wskazany tag nie istnieje </li>
     *  <li> 200 - usuwanie tagu przebiegło pomyślnie </li>
     *  <li> 403 - tag o wskazanym identyfikatorze nie należy do użytkownika żądającego usunięcia </li>
     *  </ul>
     */
    @RequestMapping(value = "/secure/tags/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteTag(@RequestAttribute("claims") Claims claims, @PathVariable("id") Long tagId){
        Optional<Tag> tagOptional = tagsRepository.findById(tagId);
        Long ownerID = new Long((Integer) claims.get("user_id"));

        if(tagOptional.isPresent()){
            Tag tag = tagOptional.get();
            if(ownerID.equals(tag.getOwner().getUserID())){
                tagsRepository.delete(tag);
                return  new ResponseEntity<>(HttpStatus.OK);
            }
            else{
                return  new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }
        }
        else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}

package com.zti.myBudget.Controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zti.myBudget.DAO.UsersRepository;
import com.zti.myBudget.BO.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import com.zti.myBudget.FormsBO.AuthUser;
import java.util.Date;
import java.util.Optional;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * Kontroler REST obsługujący operację logowania.
 */
@RestController
@RequestMapping("/auth")
public class LoginController {
    /**
     * Repozytorium użytkowników.
     */
    @Autowired
    UsersRepository usersRepository;

    /**
     * Przeprowadza procedurę logowania użytkownika w oparciu o dostarczony login i hasło.
     * Entrypoint opisywanej metody to POST["/login"].
     * Przekazane żądanie powinno zawierać:
     * <ul>
     *  <li> token autoryzacyjny </li>
     *  <li> mapowalny na obiekt POJO Set AuthUser dokument JSON  </li>
     * </ul>
     * @param login - obiekt POJO zawierający dane umożliwiające przeprowadzenie procedury logowania (email,hasło)
     * @return obiekt ResponseEntity zwracany do klienta jako HTTP response o następujących kodach:
     * <ul>
     *  <li> 401 - logowanie nie powiodło się  </li>
     *  <li> 200 - logowanie przebiegło pomyślnie </li>
     *  <li> 400 - dokument JSON wysłany w ramach żądania nie zawiera wymaganych atrybutów (login,hasło)</li>
     *  </ul>
     *  W przypadku pomyślnego przeprowadzenia procedury logowania zwracany jest dokument JSON zawierający
     *  token autoryzacyjny o określonej dacie wystawienia i identyfikatorze użytkownika.
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> login(@RequestBody AuthUser login)  {
        if (login.getEmail() == null || login.getPassword() == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        String email = login.getEmail();
        String password = login.getPassword();

        Optional<User> userOptional = usersRepository.findByEmail(email);

        if (!userOptional.isPresent()) {
            Object result = new ObjectMapper().createObjectNode().put("reason","User not exists").put("code","USER_NOT_EXIST");
            return new ResponseEntity<>(result, HttpStatus.UNAUTHORIZED);
        }

        User user = userOptional.get();

        if (!user.checkPassword(password)) {
            Object result = new ObjectMapper().createObjectNode().put("reason","Password incorrect").put("code","WRONG_PASSWORD");
            return new ResponseEntity<>(result, HttpStatus.UNAUTHORIZED);
        }

        String jwtToken = Jwts.builder().setSubject(email).claim("roles", "user")
                .claim("user_id",user.getUserID())
                .setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS256, "secretkey").compact();

        Object result = new ObjectMapper().createObjectNode().put("token", jwtToken);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}

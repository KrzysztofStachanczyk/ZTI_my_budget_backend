package com.zti.myBudget.Controllers;

import com.zti.myBudget.BO.FinanceUnit;
import com.zti.myBudget.BO.Tag;
import com.zti.myBudget.BO.User;
import com.zti.myBudget.DAO.FinanceUnitsRepository;
import com.zti.myBudget.DAO.UsersRepository;
import com.zti.myBudget.FormsBO.FinanceUnitAdd;
import com.zti.myBudget.FormsBO.FinanceUnitModify;
import com.zti.myBudget.FormsBO.NewTag;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.Optional;

/**
 * Kontroler REST obsługujący operacje na jednostkach finansowych.
 */
@RestController
public class FinanceUnitsRestController {
    /**
     * Repozytorium jednostek finansowych.
     */
    @Autowired
    FinanceUnitsRepository financeUnitsRepository;

    /**
     * Repozytorium użytkowników.
     */
    @Autowired
    UsersRepository usersRepository;

    /**
     * Zwraca listę jednostek finansowych przypisantych do konta użytkownika którego identyfikator zawarty jest w tokenie
     * dla określonej jednostki finansowej. Entrypoint opisywanej metody to GET["/secure/financeUnits"].
     * Przekazane żądanie powinno zawierać:
     * <ul>
     *  <li> token autoryzacyjny </li>
     * </ul>
     * @param claims dane z odszyfrowanego przez filtr tokenu, w tym id użytkownika, email i data wygenerowania
     * @return obiekt ResponseEntity zwracany do klienta jako HTTP response o następujących kodach:
     * <ul>
     *  <li> 404 - użytkownik nie został znalezionyh </li>
     *  <li> 200 - pobranie jednostek finansowych przebiegło pomyślnie </li>
     *  </ul>
     * W przypadku kodu 200 zwracany jest zbiór zserializowanych do dokumentów
     * JSON obiektów POJO klasy FinanceUnit.
     */
    @RequestMapping(value = "/secure/financeUnits",method = RequestMethod.GET)
    @Transactional
    public ResponseEntity<?> getAllFinanceUnits(@RequestAttribute("claims") Claims claims){
        Long userId = new Long((Integer)claims.get("user_id"));
        Optional<User> userOptional = usersRepository.findById(userId);

        if(!userOptional.isPresent()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        Object tags = userOptional.get().getFinanceUnits();
        return new ResponseEntity<>(tags,HttpStatus.OK);
    }

    /**
     * Zwraca informacje o jednostce finansowej (tj koncie) o podanym identyfikatorze.
     * Entrypoint opisywanej metody to GET["/secure/financeUnits/{id}"].
     * Przekazane żądanie powinno zawierać:
     * <ul>
     *  <li> token autoryzacyjny </li>
     *  <li> zakodowany w adresie URL identyfikator jednostki finansowej</li>
     * </ul>
     * @param claims dane z odszyfrowanego przez filtr tokenu, w tym id użytkownika, email i data wygenerowania
     * @param fuId identyfikator jednostki finansowej
     * @return obiekt ResponseEntity zwracany do klienta jako HTTP response o następujących kodach:
     * <ul>
     *  <li> 404 - jednostka finansowa nie została znaleziona </li>
     *  <li> 403 - jednostka finansowa nie należy do użytkownika którego dane zawarte są w tokenie </li>
     *  <li> 200 - pobranie jednostki finansowych przebiegło pomyślnie </li>
     *  </ul>
     * W przypadku kodu 200 zwracany jest zserializowanych do dokumentu
     * JSON obiekt POJO klasy FinanceUnit.
     */
    @RequestMapping(value = "/secure/financeUnits/{id}",method = RequestMethod.GET)
    public ResponseEntity<?> getFinanceUnitById(@RequestAttribute("claims") Claims claims, @PathVariable("id") Long fuId) {
        Optional<FinanceUnit> fuOptional = financeUnitsRepository.findById(fuId);
        Long ownerID = new Long((Integer) claims.get("user_id"));

        if(fuOptional.isPresent()){
            FinanceUnit fu = fuOptional.get();
            if(ownerID.equals(fu.getOwner().getUserID())){
                return new ResponseEntity<>(fu,HttpStatus.OK);
            }
            else {
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }
        }
        else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Modyfikuje jednostę finansową o wskazanym identyfikatorze.
     * Entrypoint opisywanej metody to PUT["/secure/financeUnits/{id}"].
     * Przekazane żądanie powinno zawierać:
     * <ul>
     *  <li> token autoryzacyjny </li>
     *  <li> zakodowany w adresie URL identyfikator jednostki finansowej</li>
     *  <li> mapowalny na obiekt POJO Set &lt; FinanceUnitModify &gt; dokument JSON </li>
     * </ul>
     * @param claims dane z odszyfrowanego przez filtr tokenu, w tym id użytkownika, email i data wygenerowania
     * @param modifiedFinanceUnit obiekt POJO FinanceUnitModify reprezentujący atrybuty zmodyfikowanej jednostki finansowej
     * @param fuId identyfikator jednostki finansowej
     * @return obiekt ResponseEntity zwracany do klienta jako HTTP response o następujących kodach:
     * <ul>
     *  <li> 404 - modyfikowana jednostka finansowa nie została znaleziona </li>
     *  <li> 403 - modyfikowana jednostka finansowa nie należy do użytkownika którego dane zawarte są w tokenie </li>
     *  <li> 400 - błąd walidacji, odwzorowania JSON- &gt; POJO </li>
     *  <li> 200 - modyfikacja jednostki finansowej przebiegło pomyślnie </li>
     *  <li> 409 - w bazie danych występuje już jednostka finansowa o takiej samej nazwie i właścicielu </li>
     *  </ul>
     */
    @RequestMapping(value = "/secure/financeUnits/{id}",method = RequestMethod.PUT)
    public ResponseEntity<?> modifyFinanceUnit(@RequestAttribute("claims") Claims claims,@Valid @RequestBody FinanceUnitModify modifiedFinanceUnit,@PathVariable("id") Long fuId){
        Optional<FinanceUnit> fuOptional = financeUnitsRepository.findById(fuId);
        Long ownerID = new Long((Integer) claims.get("user_id"));

        if(fuOptional.isPresent()) {
            FinanceUnit fu = fuOptional.get();
            if(ownerID.equals(fu.getOwner().getUserID())){
                fu.setName(modifiedFinanceUnit.getName());
                fu.setDescription(modifiedFinanceUnit.getDescription());
                try {
                    financeUnitsRepository.saveAndFlush(fu);
                }
                catch (org.springframework.dao.DataIntegrityViolationException e){
                    return ResponseEntity.status(HttpStatus.CONFLICT).build();
                }
                return ResponseEntity.status(HttpStatus.OK).build();
            }
            else{
                return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }
        }
        else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    /**
     * Dodaje jednostkę finansową do konta użytkownika o identyfikatorze zawartym w tokenie.
     * Entrypoint opisywanej metody to POST["/secure/financeUnits"].
     * Przekazane żądanie powinno zawierać:
     * <ul>
     *  <li> token autoryzacyjny </li>
     *  <li> mapowalny na obiekt POJO Set &lt; FinanceUnitAdd &gt; dokument JSON </li>
     * </ul>
     * @param claims dane z odszyfrowanego przez filtr tokenu, w tym id użytkownika, email i data wygenerowania
     * @param newFu obiekt POJO FinanceUnitAdd reprezentujący atrybuty nowej jednostki finansowej
     * @return obiekt ResponseEntity zwracany do klienta jako HTTP response o następujących kodach:
     * <ul>
     *  <li> 404 - użytkownik o id zawartym w tokenie nie został znaleziony </li>
     *  <li> 400 - błąd walidacji, odwzorowania JSON- &gt; POJO </li>
     *  <li> 200 - dodawanie jednostki finansowej przebiegło pomyślnie </li>
     *  <li> 409 - w bazie danych występuje już jednostka finansowa o takiej samej nazwie i właścicielu </li>
     *  </ul>
     */
    @RequestMapping(value = "/secure/financeUnits",method = RequestMethod.POST)
    public ResponseEntity<?> insertNewFinanceUnit(@RequestAttribute("claims") Claims claims,@Valid @RequestBody FinanceUnitAdd newFu){
        Long ownerID = new Long((Integer) claims.get("user_id"));
        Optional<User> userOptional = usersRepository.findById(ownerID);

        if(!userOptional.isPresent()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        FinanceUnit fu = new FinanceUnit(null,newFu.getName(),userOptional.get(),newFu.getDescription());

        try {
            financeUnitsRepository.saveAndFlush(fu);
        }catch (org.springframework.dao.DataIntegrityViolationException e){
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }

        return new ResponseEntity<>(fu,HttpStatus.OK);
    }

    /**
     * Usuwa jednostkę finansową o wskazanym identyfikatorze.
     * Entrypoint opisywanej metody to DELETE["/secure/financeUnits/{id}"].
     * Przekazane żądanie powinno zawierać:
     * <ul>
     *  <li> token autoryzacyjny </li>
     *  <li> zakodowany w adresie URL identyfikator jednostki finansowej</li>
     *  </ul>
     * @param claims dane z odszyfrowanego przez filtr tokenu, w tym id użytkownika, email i data wygenerowania
     * @param fuId identyfikator jednostki finansowej przeznaczonej do usunięcia
     * @return obiekt ResponseEntity zwracany do klienta jako HTTP response o następujących kodach:
     * <ul>
     *  <li> 404 - jednostka finansowa o zadanym id nie został znaleziony </li>
     *  <li> 403 - jednostka finansowa nie należy do użytkownika którego dane zawarte są w tokenie </li>
     *  <li> 200 - usuwanie jednostki finansowej przebiegło pomyślnie </li>
     *  </ul>
     */
    @RequestMapping(value = "/secure/financeUnits/{id}", method = RequestMethod.DELETE)
    @Transactional
    public ResponseEntity<?> deleteFinanceUnit(@RequestAttribute("claims") Claims claims, @PathVariable("id") Long fuId){

        Optional<FinanceUnit> fuOptional = financeUnitsRepository.findById(fuId);
        Long ownerID = new Long((Integer) claims.get("user_id"));

        if(fuOptional.isPresent()){
            FinanceUnit fu = fuOptional.get();
            if(ownerID.equals(fu.getOwner().getUserID())){
                financeUnitsRepository.delete(fu);
                return  new ResponseEntity<>(HttpStatus.OK);
            }
            else{
                return  new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }
        }
        else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}

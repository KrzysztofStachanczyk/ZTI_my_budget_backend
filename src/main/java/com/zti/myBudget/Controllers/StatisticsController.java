package com.zti.myBudget.Controllers;

import com.zti.myBudget.BO.FinanceUnit;
import com.zti.myBudget.DAO.FinanceUnitsRepository;
import com.zti.myBudget.DAO.StatisticsRepository;
import com.zti.myBudget.QueryBO.BasicStatisticsOfOperationsTimeEvolution;
import com.zti.myBudget.QueryBO.BasicStatisticsOfTag;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Optional;

/**
 * Kontroler REST obsługujący żądania wygenerowania statystyk.
 */
@Controller
public class StatisticsController {
    /**
     * Repozytorium jednostek finansowych.
     */
    @Autowired
    FinanceUnitsRepository financeUnitsRepository;

    /**
     * Repozytorium statystyk.
     */
    @Autowired
    StatisticsRepository statisticsRepository;

    /**
     * Pobiera statystyki dotyczące poszczególnych tagów w ramach wskazanej jednostki finansowej.
     * Entrypoint opisywanej metody to GET["/secure/statistics/{id}/basicStatisticsPerTag"].
     * Przekazane żądanie powinno zawierać:
     * <ul>
     *  <li> token autoryzacyjny </li>
     *  <li> zapisany w adresie URL identyfikator jednostki finansowej  </li>
     * </ul>
     * @param claims  dane z odszyfrowanego przez filtr tokenu, w tym id użytkownika, email i data wygenerowania
     * @param fuId identyfikator jednostki finansowej
     * @return obiekt ResponseEntity zwracany do klienta jako HTTP response o następujących kodach:
     * <ul>
     *  <li> 404 - jednostka finansowa nie została znaleziona </li>
     *  <li> 403 - jednostka finansowa nie należy do użytkownika którego dane zawarte są w tokenie </li>
     *  <li> 200 - pobranie statystyk przebiegło pomyślnie </li>
     *  </ul>
     * W przypadku kodu 200 zwracana jest lista zserializowanych do dokumentów
     * JSON obiektów POJO klasy BasicStatisticsOfTag zawierających statystyki tagów.
     */
    @RequestMapping(value = "/secure/statistics/{id}/basicStatisticsPerTag", method = RequestMethod.GET)
    public ResponseEntity<?> getBasicStatisticsPerTag(@RequestAttribute("claims") Claims claims, @PathVariable("id") Long fuId) {
        Optional<FinanceUnit> financeOperationOptional = financeUnitsRepository.findById(fuId);
        Long ownerID = new Long((Integer) claims.get("user_id"));

        if(!financeOperationOptional.isPresent()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        if(!ownerID.equals(financeOperationOptional.get().getOwner().getUserID())){
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        List<BasicStatisticsOfTag> result = statisticsRepository.findBasicStatisticsOfTags(fuId);
        return new ResponseEntity<>(result,HttpStatus.OK);
    }


    /**
     * Pobiera statystyki dotyczące poszczególnych tagów w ramach wskazanej jednostki finansowej w zadanym oknie czasowym.
     * Entrypoint opisywanej metody to GET["/secure/statistics/{id}/basicStatisticsPerTag/timeRange/{from}/{to}"].
     * Przekazane żądanie powinno zawierać:
     * <ul>
     *  <li> token autoryzacyjny </li>
     *  <li> zapisany w adresie URL identyfikator jednostki finansowej  </li>
     *  <li> zapisane w adresie URL daty początkowa i końcowa analizowanego okna czasowego  </li>
     * </ul>
     * @param claims  dane z odszyfrowanego przez filtr tokenu, w tym id użytkownika, email i data wygenerowania
     * @param fuId identyfikator jednostki finansowej
     * @param fromString data początkowa w formacie YYYY-MM-DD
     * @param toString data końcowa w formacie YYYY-MM-DD
     * @return obiekt ResponseEntity zwracany do klienta jako HTTP response o następujących kodach:
     * <ul>
     *  <li> 404 - jednostka finansowa nie została znaleziona </li>
     *  <li> 403 - jednostka finansowa nie należy do użytkownika którego dane zawarte są w tokenie </li>
     *  <li> 200 - pobranie statystyk przebiegło pomyślnie </li>
     *  </ul>
     * W przypadku kodu 200 zwracana jest lista zserializowanych do dokumentów
     * JSON obiektów POJO klasy BasicStatisticsOfTag zawierających statystyki tagów.
     */
    @RequestMapping(value = "/secure/statistics/{id}/basicStatisticsPerTag/timeRange/{from}/{to}", method = RequestMethod.GET)
    public ResponseEntity<?> getBasicStatisticsPerTagInTimeRange(@RequestAttribute("claims") Claims claims, @PathVariable("id") Long fuId,
    @PathVariable("from") String fromString, @PathVariable("to") String toString) {

        LocalDate from;
        LocalDate to;

        try{
            from =  LocalDate.parse(fromString);
            to = LocalDate.parse(toString);
        }catch(DateTimeParseException e){
            return  new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Optional<FinanceUnit> financeOperationOptional = financeUnitsRepository.findById(fuId);
        Long ownerID = new Long((Integer) claims.get("user_id"));

        if(!financeOperationOptional.isPresent()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        if(!ownerID.equals(financeOperationOptional.get().getOwner().getUserID())){
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        List<BasicStatisticsOfTag> result = statisticsRepository.findBasicStatisticsInTimeRange(fuId,from,to);
        return new ResponseEntity<>(result,HttpStatus.OK);
    }

    /**
     * Pobiera zagregowane informacje o ilości operacji, bilansie, przychodzie i rozchodzie w ramach
     * jednostkie finansowej o przekazanym identyfikatorze. Entrypoint opisywanej metody to GET["/secure/statistics/{id}/basicTimeEvolution/{period}"].
     * Przekazane żądanie powinno zawierać:
     * <ul>
     *  <li> token autoryzacyjny </li>
     *  <li> zapisany w adresie URL identyfikator jednostki finansowej  </li>
     *  <li> zapisany w adresie URL nazwę okresu po którym agregowane są dane ("day","week","month","quarter","year") </li>
     * </ul>
     *
     * @param claims  dane z odszyfrowanego przez filtr tokenu, w tym id użytkownika, email i data wygenerowania
     * @param fuId identyfikator jednostki finansowej
     * @param periodName nazwa okresu po którym grupowane są dane
     * @return obiekt ResponseEntity zwracany do klienta jako HTTP response o następujących kodach:
     * <ul>
     *  <li> 404 - jednostka finansowa nie została znaleziona </li>
     *  <li> 403 - jednostka finansowa nie należy do użytkownika którego dane zawarte są w tokenie </li>
     *  <li> 200 - pobranie statystyk przebiegło pomyślnie </li>
     *  <li> 501 - nazwa okresu nie jest obsługiwana</li>
     *  </ul>
     * W przypadku kodu 200 zwracana jest lista zserializowanych do dokumentów
     * JSON obiektów POJO klasy BasicStatisticsOfOperationsTimeEvolution. Lista ta jest posortowana po czasie.
     */
    @RequestMapping(value = "/secure/statistics/{id}/basicTimeEvolution/{period}", method = RequestMethod.GET)
    public ResponseEntity<?> getBasicTimeEvolution(@RequestAttribute("claims") Claims claims, @PathVariable("id") Long fuId,
        @PathVariable("period") String periodName) {

        Optional<FinanceUnit> financeOperationOptional = financeUnitsRepository.findById(fuId);
        Long ownerID = new Long((Integer) claims.get("user_id"));

        if(!financeOperationOptional.isPresent()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        if(!ownerID.equals(financeOperationOptional.get().getOwner().getUserID())){
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        if(!(
                periodName.equalsIgnoreCase("day") ||
                periodName.equalsIgnoreCase("week") ||
                periodName.equalsIgnoreCase("month") ||
                periodName.equalsIgnoreCase("quarter") ||
                periodName.equalsIgnoreCase("year")
        )){
            return  new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
        }

        List<BasicStatisticsOfOperationsTimeEvolution> result = statisticsRepository.findEvolutionBasic(fuId,periodName);
        return new ResponseEntity<>(result,HttpStatus.OK);
    }
}

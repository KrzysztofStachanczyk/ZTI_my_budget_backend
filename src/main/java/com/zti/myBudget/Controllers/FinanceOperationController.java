package com.zti.myBudget.Controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zti.myBudget.BO.FinanceOperation;
import com.zti.myBudget.BO.FinanceUnit;
import com.zti.myBudget.BO.Tag;
import com.zti.myBudget.DAO.FinanceOperationsRepository;
import com.zti.myBudget.DAO.FinanceUnitsRepository;
import com.zti.myBudget.DAO.TagsRepository;
import com.zti.myBudget.FormsBO.FinanceOperationAddOrModify;
import com.zti.myBudget.QueryBO.FinanceOperationBasic;
import com.zti.myBudget.QueryBO.FinanceOperationBasicWithDesc;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Kontroler REST obsługujący operacje na operacjach finansowych.
 */
@RestController
public class FinanceOperationController {
    /**
     * Repozytorium jednostek finansowych.
     */
    @Autowired
    FinanceUnitsRepository financeUnitsRepository;

    /**
     * Repozytorium operacji finansowych.
     */
    @Autowired
    FinanceOperationsRepository financeOperationsRepository;

    /**
     * Repozytorium tagów.
     */
    @Autowired
    TagsRepository tagsRepository;

    /**
     * Przypisuje wybrane tagi do operacji finansowej.
     * Entrypoint opisywanej metody to PUT["/secure/operations/{id}/tags"].
     * Przekazane żądanie powinno zawierać:
     * <ul>
     *  <li> mapowalny na obiekt POJO Set &lt; Long &gt; dokument JSON </li>
     *  <li> token autoryzacyjny </li>
     *  <li> zakodowany w adresie URL identyfikator operacji finansowej </li>
     * </ul>
     * @param claims dane z odszyfrowanego przez filtr tokenu, w tym id użytkownika, email i data wygenerowania
     * @param tagsIDsToAppend zbiór identyfikatorów tagów, które mają zostać przypisane do operacji finansowej
     * @param opId identyfikator operacji finansowej
     * @return obiekt ResponseEntity zwracany do klienta jako HTTP response o następujących kodach:
     * <ul>
     *  <li> 404 - operacja finansowa nie została znaleziona </li>
     *  <li> 403 - operacja finansowa nie należy do użytkownika którego dane zawarte są w tokenie </li>
     *  <li> 200 - przypisanie tagów przebiegło pomyślnie </li>
     *  <li> 400 - błąd walidacji, odwzorowania JSON- &gt; POJO </li>
     *  </ul>
     */
    @RequestMapping(value = "/secure/operations/{id}/tags",method = RequestMethod.PUT)
    public ResponseEntity<?> setUpTagsAssignmentForOperation(@RequestAttribute("claims") Claims claims, @Valid @RequestBody Set<Long> tagsIDsToAppend, @PathVariable("id") Long opId) {
        int total = 0;
        int connected = 0;

        Long ownerID = new Long((Integer) claims.get("user_id"));

        Optional<FinanceOperation> financeOperationOptional = financeOperationsRepository.findById(opId);

        if(!financeOperationOptional.isPresent()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        FinanceOperation financeOperation = financeOperationOptional.get();

        if(!ownerID.equals(financeOperation.getFinanceUnit().getOwner().getUserID())){
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        Set<Tag> tags = new HashSet<>();

        for (Long tagId: tagsIDsToAppend) {
            Optional<Tag> tagOptional = tagsRepository.findById(tagId);
            if(tagOptional.isPresent() && ownerID.equals(tagOptional.get().getOwner().getUserID())){
                tags.add(tagOptional.get());
                connected++;
            }
            total++;
        }

        financeOperation.setTags(tags);
        financeOperationsRepository.saveAndFlush(financeOperation);

        Object result = new ObjectMapper().createObjectNode().put("total",total).put("connected",connected);
        return new ResponseEntity<>(result,HttpStatus.OK);
    }

    /**
     * Importuje dostarczony zbiór operacji finansowych do zadanej jednostki finansowej. Duplikaty są ignorowane.
     * Entrypoint opisywanej metody to POST["/secure/financeUnits/{id}/operations/import"] Przekazane żądanie powinno zawierać:
     * <ul>
     *  <li> mapowalny na obiekt POJO Set &lt; FinanceOperationAddOrModify &gt; dokument JSON </li>
     *  <li> token autoryzacyjny </li>
     *  <li> zakodowany w adresie URL identyfikator jednostki finansowej </li>
     * </ul>
     * @param claims dane z odszyfrowanego przez filtr tokenu, w tym id użytkownika, email i data wygenerowania
     * @param newOperations zbiór operacji finansowych, które powinny zostać zaimportowane
     * @param fuId identyfikator jednostki finansowej
     * @return obiekt ResponseEntity zwracany do klienta jako HTTP response o następujących kodach:
     * <ul>
     *  <li> 404 - jednostka finansowa nie została znaleziona </li>
     *  <li> 403 - jednostka finansowa nie należy do użytkownika którego dane zawarte są w tokenie </li>
     *  <li> 400 - błąd walidacji, odwzorowania JSON- &gt; POJO </li>
     *  <li> 200 - import operacji finansowych przebiegł pomyślnie </li>
     *  </ul>
     *  Oprócz tego w przypadku wygenerowania kodu 200 zwracany jest dokument json z atrybutami:
     *  <ul>
     *      <li> total - ilość wysłanych opercji finansowych</li>
     *      <li> inserted - liczba poprawnie zaimportowanych operacji finansowych</li>
     *  </ul>
     *  */
    @RequestMapping(value = "/secure/financeUnits/{id}/operations/import", method = RequestMethod.POST)
    public ResponseEntity<?> importOperations (@RequestAttribute("claims") Claims claims, @Valid @RequestBody Set<FinanceOperationAddOrModify> newOperations, @PathVariable("id") Long fuId) {
        Optional<FinanceUnit> fuOptional = financeUnitsRepository.findById(fuId);
        Long ownerID = new Long((Integer) claims.get("user_id"));

        if (fuOptional.isPresent()) {
            FinanceUnit fu = fuOptional.get();
            if (ownerID.equals(fu.getOwner().getUserID())) {
                long total = 0;
                long inserted = 0;

                for (FinanceOperationAddOrModify fo:newOperations) {
                    try{
                        FinanceOperation financeOperation = new FinanceOperation(fo,fu);
                        financeOperationsRepository.saveAndFlush(financeOperation);
                        inserted++;
                    } catch (org.springframework.dao.DataIntegrityViolationException ignored){}
                    total++;
                }
                Object result = new ObjectMapper().createObjectNode().put("total",total).put("inserted",inserted);
                return new ResponseEntity<>(result,HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }
        }else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Zwraca listę informacji o operacjach finansowych (List &lt; FinanceOperationBasicWithDesc &gt; ) w wybranym przedziale czasowym i
     * dla określonej jednostki finansowej.
     * Entrypoint opisywanej metody to GET["/secure/financeUnits/{id}/operations/timeRange/{from}/{to}"] Przekazane żądanie powinno zawierać:
     * <ul>
     *  <li> token autoryzacyjny </li>
     *  <li> zakodowany w adresie URL identyfikator jednostki finansowej </li>
     *  <li> zakodowane w adresie URL daty początkowa i końcowa pobieranego zakresu danych w formacie YYYY-MM-DD </li>
     * </ul>
     * @param claims dane z odszyfrowanego przez filtr tokenu, w tym id użytkownika, email i data wygenerowania
     * @param fuId identyfikator jednostki finansowej
     * @param fromString data początkowa
     * @param toString data końcowa
     * @return obiekt ResponseEntity zwracany do klienta jako HTTP response o następujących kodach:
     * <ul>
     *     <li>400 - błąd konwersji fromString lub toString na obiekt LocalDate</li>
     *     <li>404 - jednostka finansowa o zadanym id nie została znaleziona </li>
     *     <li>403 - jednostka finansowa nie należy do użytkownika którego dane zawarte są w tokenie </li>
     *     <li>200 - żądanie zostało poprawnie obsłużone</li>
     * </ul>
     * W przypadku kodu 200 zwracana jest posortowana po dacie lista zserializowanych do tablicy dokumentów
     * JSON obiektów POJO FinanceOperationBasicWithDesc.
     */
    @RequestMapping(value = "/secure/financeUnits/{id}/operations/timeRange/{from}/{to}", method = RequestMethod.GET)
    public ResponseEntity<?> getAllOperationsInTimeRange(@RequestAttribute("claims") Claims claims, @PathVariable("id") Long fuId,
        @PathVariable("from") String fromString, @PathVariable("to") String toString) {
        LocalDate from;
        LocalDate to;

        try{
            from =  LocalDate.parse(fromString);
            to = LocalDate.parse(toString);
        }catch(DateTimeParseException e){
            return  new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Optional<FinanceUnit> fuOptional = financeUnitsRepository.findById(fuId);
        Long ownerID = new Long((Integer) claims.get("user_id"));

        if (fuOptional.isPresent()) {
            FinanceUnit fu = fuOptional.get();
            if (ownerID.equals(fu.getOwner().getUserID())) {
                List<FinanceOperationBasicWithDesc> result = financeOperationsRepository.findAllOperationsInTimeRange(fu.getId(),from,to);
                return new ResponseEntity<>(result, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Zwraca zbiór informacji o operacjach finansowych (Set &lt; FinanceOperationBasic &gt; )
     * dla określonej jednostki finansowej.
     * Entrypoint opisywanej metody to GET["/secure/financeUnits/{id}/operations"] Przekazane żądanie powinno zawierać:
     * <ul>
     *  <li> token autoryzacyjny </li>
     *  <li> zakodowany w adresie URL identyfikator jednostki finansowej </li>
     * </ul>
     * @param claims dane z odszyfrowanego przez filtr tokenu, w tym id użytkownika, email i data wygenerowania
     * @param fuId identyfikator jednostki finansowej
     * @return obiekt ResponseEntity zwracany do klienta jako HTTP response o następujących kodach:
     * <ul>
     *     <li>404 - jednostka finansowa o zadanym id nie została znaleziona </li>
     *     <li>403 - jednostka finansowa nie należy do użytkownika którego dane zawarte są w tokenie </li>
     *     <li>200 - żądanie zostało poprawnie obsłużone</li>
     * </ul>
     * W przypadku kodu 200 zwracany jest zbiór zserializowanych do tablicy dokumentów
     * JSON obiektów POJO FinanceOperationBasic.
     */
    @RequestMapping(value = "/secure/financeUnits/{id}/operations", method = RequestMethod.GET)
    public ResponseEntity<?> getAllOperationsByFinanceUnitId(@RequestAttribute("claims") Claims claims, @PathVariable("id") Long fuId) {
        Optional<FinanceUnit> fuOptional = financeUnitsRepository.findById(fuId);
        Long ownerID = new Long((Integer) claims.get("user_id"));

        if (fuOptional.isPresent()) {
            FinanceUnit fu = fuOptional.get();
            if (ownerID.equals(fu.getOwner().getUserID())) {
                Set<FinanceOperationBasic> result = financeOperationsRepository.findAllOperationsBasicInfo(fu.getId());
                return new ResponseEntity<>(result, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Zwraca pełny opis operacji finansowej o zadanym identyfikatorze.
     * Entrypoint opisywanej metody to GET["/secure/operations/{id}"] Przekazane żądanie powinno zawierać:
     * <ul>
     *  <li> token autoryzacyjny </li>
     *  <li> zakodowany w adresie URL identyfikator operacji finansowej </li>
     * </ul>
     * @param claims dane z odszyfrowanego przez filtr tokenu, w tym id użytkownika, email i data wygenerowania
     * @param opId identyfikator operacji finansowej
     * @return obiekt ResponseEntity zwracany do klienta jako HTTP response o następujących kodach:
     * <ul>
     *     <li>404 - operacja  o zadanym id nie została znaleziona </li>
     *     <li>403 - operacja  nie należy do użytkownika którego dane zawarte są w tokenie </li>
     *     <li>200 - żądanie zostało poprawnie obsłużone</li>
     * </ul>
     * W przypadku kodu 200 zwracany  zserializowanych do dokumentu
     * JSON obiekt POJO FinanceOperation.
     */
    @RequestMapping(value = "/secure/operations/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getByOperationById(@RequestAttribute("claims") Claims claims, @PathVariable("id") Long opId) {
        Optional<FinanceOperation> financeOperationOptional = financeOperationsRepository.findById(opId);
        Long ownerID = new Long((Integer) claims.get("user_id"));

        if (financeOperationOptional.isPresent()) {
            FinanceOperation financeOperation = financeOperationOptional.get();
            if (ownerID.equals(financeOperation.getFinanceUnit().getOwner().getUserID())) {
                return new ResponseEntity<>(financeOperation, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Dodaje operacje do jednostki finansowej o wskazanym identyfikatorze.
     * Entrypoint opisywanej metody to POST["/secure/financeUnits/{id}/operations"]. Przekazane żądanie powinno zawierać:
     * <ul>
     *  <li> token autoryzacyjny </li>
     *  <li> zakodowany w adresie URL identyfikator jednostki finansowej </li>
     *  <li> mapowalny na obiekt POJO Set &lt; FinanceOperationAddOrModify &gt; dokument JSON </li>
     * </ul>
     * @param claims dane z odszyfrowanego przez filtr tokenu, w tym id użytkownika, email i data wygenerowania
     * @param newFo obiekt POJO FinanceOperationAddOrModify reprezentujący nową operację finansową
     * @param fuId identyfikator jednostri finansowej
     * @return obiekt ResponseEntity zwracany do klienta jako HTTP response o następujących kodach:
     * <ul>
     *  <li> 404 - jednostka finansowa nie została znaleziona </li>
     *  <li> 403 - jednostka finansowa nie należy do użytkownika którego dane zawarte są w tokenie </li>
     *  <li> 400 - błąd walidacji, odwzorowania JSON- &gt; POJO </li>
     *  <li> 200 - dodanie operacji finansowych przebiegło pomyślnie </li>
     *  <li> 409 - w bazie danych występuje już podobna operacja finansowa </li>
     *  </ul>
     */
    @RequestMapping(value = "/secure/financeUnits/{id}/operations", method = RequestMethod.POST)
    public ResponseEntity<?> addOperation(@RequestAttribute("claims") Claims claims, @Valid @RequestBody FinanceOperationAddOrModify newFo, @PathVariable("id") Long fuId) {
        Long ownerID = new Long((Integer) claims.get("user_id"));
        Optional<FinanceUnit> financeUnitOptional = financeUnitsRepository.findById(fuId);

        if (!financeUnitOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        FinanceUnit financeUnit = financeUnitOptional.get();

        if (!ownerID.equals(financeUnit.getOwner().getUserID())) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        FinanceOperation financeOperation = new FinanceOperation(newFo, financeUnit);

        try {
            financeOperationsRepository.saveAndFlush(financeOperation);
        } catch (org.springframework.dao.DataIntegrityViolationException e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }

        return ResponseEntity.status(HttpStatus.OK).build();
    }

    /**
     * Usuwa wskazaną operację finansową.
     * Entrypoint opisywanej metody to DELETE["/secure/operations/{id}"]. Przekazane żądanie powinno zawierać:
     * <ul>
     *  <li> token autoryzacyjny </li>
     *  <li> zakodowany w adresie URL identyfikator operacji finansowej </li>
     * </ul>
     * @param claims dane z odszyfrowanego przez filtr tokenu, w tym id użytkownika, email i data wygenerowania
     * @param operationID identyfikator operacji finansowej
     * @return obiekt ResponseEntity zwracany do klienta jako HTTP response o następujących kodach:
     * <ul>
     *     <li>404 - operacja  o zadanym id nie została znaleziona </li>
     *     <li>403 - operacja  nie należy do użytkownika którego dane zawarte są w tokenie </li>
     *     <li>200 - żądanie zostało poprawnie obsłużone</li>
     * </ul>
     */
    @RequestMapping(value = "/secure/operations/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteOperationById(@RequestAttribute("claims") Claims claims, @PathVariable("id") Long operationID) {
        Long ownerID = new Long((Integer) claims.get("user_id"));

        Optional<FinanceOperation> financeOperationOptional = financeOperationsRepository.findById(operationID);

        if (!financeOperationOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        FinanceOperation financeOperation = financeOperationOptional.get();
        if (ownerID.equals(financeOperation.getFinanceUnit().getOwner().getUserID())) {
            financeOperationsRepository.delete(financeOperation);
            return ResponseEntity.status(HttpStatus.OK).build();
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }
}
package com.zti.myBudget.Controllers;

import java.util.ArrayList;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zti.myBudget.FormsBO.RegisterUser;
import  com.zti.myBudget.BO.User;
import com.zti.myBudget.DAO.UsersRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

/**
 * Kontroler REST obsługujący konta użytkowników.
 */
@RestController
public class UserRestController {
    /**
     * Repozytorium użytkowników
     */
    @Autowired
    UsersRepository usersRepository;

    /**
     * Tworzy użytkownika na podstawie danych dostarczonych w żądaniu.
     * Entrypoint opisywanej metody to POST["/user"].
     * Przekazane żądanie powinno zawierać:
     * <ul>
     *  <li> token autoryzacyjny </li>
     *  <li> mapowalny na obiekt POJO klasy RegisterUser dokument JSON </li>
     * </ul>
     * @param newUser - obiekt POJO reprezentujący nowego użytkownika. Zawiera on informację o haśle,emailu, imieniu i nazwisku.
     * @return obiekt ResponseEntity zwracany do klienta jako HTTP response o następujących kodach:
     * <ul>
     *  <li> 200 - tworzenie użytkownika przebiegło pomyślnie </li>
     *  <li> 409 - użytkownik o podanym identyfikatorze już istnieje  </li>
     *  <li> 400 - błąd walidacji, odwzorowania JSON- &gt; POJO </li>
     *  </ul>
     *  W przypadku pomyślnej rejestracji zwracany jest dokument JSON zawierający
     *  token autoryzacyjny o określonej dacie wystawienia i identyfikatorze nowego użytkownika.
     */
    @RequestMapping(value = "/user",method =  RequestMethod.POST)
    public ResponseEntity<?> add(@Valid @RequestBody RegisterUser newUser) {
        try {
            User user = usersRepository.saveAndFlush(new User(newUser));

            String jwtToken = Jwts.builder().setSubject(user.getEmail()).claim("roles", "user")
                    .claim("user_id",user.getUserID())
                    .setIssuedAt(new Date())
                    .signWith(SignatureAlgorithm.HS256, "secretkey").compact();

            Object result = new ObjectMapper().createObjectNode().put("token", jwtToken);
            return new ResponseEntity<Object>(result,HttpStatus.CREATED);

        }catch (org.springframework.dao.DataIntegrityViolationException e){
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
    }

    /**
     * Zwraca szczegółowe informacje o użytkowniku, którego dane znajdują się w tokenie.
     * Entrypoint opisywanej metody to GET["/secure/user"].
     * Przekazane żądanie powinno zawierać:
     * <ul>
     *  <li> token autoryzacyjny </li>
     * </ul>
     * @param claims dane z odszyfrowanego przez filtr tokenu, w tym id użytkownika, email i data wygenerowania
     * @return obiekt ResponseEntity zwracany do klienta jako HTTP response o następujących kodach:
     * <ul>
     *  <li> 200 - pomyślnie pobrano dane użytkownika  </li>
     *  <li> 404 - użytkownik o podanym w tokenie identyfikatorze nie istnieje </li>
     *  <li> 400 - błąd walidacji, odwzorowania JSON- &gt; POJO </li>
     *  </ul>
     *  W przypadku pomyślnego pobrania informacji o użytkowniku są one zwracane w formie dokumentu JSON
     *  uzyskanego w wyniku serializacji obiektu POJO User
     */

    @RequestMapping(value = "/secure/user/",method = RequestMethod.GET)
    public ResponseEntity<?> getUserDetails(@RequestAttribute("claims") Claims claims){
        Object user =usersRepository.findById((Long) claims.get("user_id"));

        if (user==null){
            return  new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Object>(user,HttpStatus.OK);
    }



}

package com.zti.myBudget.Aspects;

import com.zti.myBudget.BO.ControllersCalls;
import com.zti.myBudget.DAO.ControllersCallsRepository;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StopWatch;

import java.util.Optional;

/**
 * Komponent zbierający statystyki związane z pracą kontrolerów restowych. Wykorzystuje elementy programowania aspektowego.
 */
@Aspect
@Component
public class ControllersMonitoring {
    /**
     * Repozytorium statystyk kontrolerów.
     */
    @Autowired
    ControllersCallsRepository controllersCallsRepository;


    /**
     * Metoda obsługujaca punkt przecięcia polegający na wywołaniu publicznych metod kontrolerów REST.
     * @param joinPoint punkt złączenia
     * @return HTTP response zgodny z tym zwracanym przez kontroler
     * @throws Throwable wyjątek wyrzucany przez kontroler - jest on przechwytywany i rzucany ponownie po zapisie statystyk.
     */
    @Around("execution(* com.zti.myBudget.Controllers.*.*(..))")
    public Object logTimeMethod(ProceedingJoinPoint joinPoint) throws Throwable {
        Throwable exception = null;
        Object returnValue = null;

        StopWatch stopWatch = new StopWatch();

        stopWatch.start();

        try {
            returnValue = joinPoint.proceed();
        }catch (Throwable localException){
            exception=localException;
        }

        stopWatch.stop();

        String path = joinPoint.getTarget().getClass().getName() + "." + joinPoint.getSignature().getName();

        // bledy na tym etapie nie powinny unieruchamiac aplikacji
        try{
            incrementCallsCounter(path,exception!=null,stopWatch.getTotalTimeSeconds());
        }catch (Exception ignored){
        }

        if(exception!=null){
            throw exception;
        }
        else {
            return returnValue;
        }
    }

    /**
     * Aktualizuje statystyki wykorzystania kontrolerów.
     * @param path nazwa pakiety,klasy i wywoływanej metody
     * @param exceptionCatch czyh kontroler wyrzucił wyjątek
     * @param execTime czas pracy kontrolera w sekundach
     */
    @Transactional(propagation = Propagation.REQUIRED)
    protected void incrementCallsCounter(String path,boolean exceptionCatch,double execTime){
        Optional<ControllersCalls> optionalControllersCalls = controllersCallsRepository.findById(path);

        ControllersCalls calls;
        if(optionalControllersCalls.isPresent()){
            calls = optionalControllersCalls.get();
        }
        else{
            calls = new ControllersCalls(path);
        }

        if (calls.getCallNumber() == 0){
            calls.setAverageExecutionTime(execTime);
        }
        else{
            double N = calls.getCallNumber();
            calls.setAverageExecutionTime(calls.getAverageExecutionTime()*((N-1)/N) + execTime/N);
        }

        if(exceptionCatch){
            calls.setExceptionsNumber(calls.getExceptionsNumber() + 1);
        }

        calls.setCallNumber(calls.getCallNumber() + 1);

        controllersCallsRepository.save(calls);
    }
}
